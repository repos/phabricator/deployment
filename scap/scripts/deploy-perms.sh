#!/bin/bash
# SPDX-License-Identifier: Apache-2.0
#
# First, resets permissions for the entire phabricator deployment tree to avoid
# issues with scap manipulation of files.
#
# Next, ensures that files under phabricator/conf/local/ and
# phabricator/support/ have the right group ownership and permissions. This
# script is intended to run following the config_deploy stage of phabricator
# deployment via scap deploy.

set -euo pipefail

if [ -z "$SCAP_REV_PATH" ]; then
  echo '$SCAP_REV_PATH is not defined.'
  echo 'Note: This script is only intended to run as a scap deploy check'
  exit 1
fi

sudo find /srv/deployment/phabricator -not -user deploy-devtools -print0 | xargs -0 sudo chown deploy-devtools:wikidev
sudo chmod -R u+w /srv/deployment/phabricator/deployment/phabricator/conf/local
sudo chmod u+w /srv/deployment/phabricator/deployment/phabricator/support/redirect_config.json

sudo chgrp mail "$SCAP_REV_PATH"/phabricator/conf/local/mail.json
sudo chgrp phd "$SCAP_REV_PATH"/phabricator/conf/local/phd.json
sudo chgrp phd "$SCAP_REV_PATH"/phabricator/conf/local/vcs.json
sudo chgrp www-data "$SCAP_REV_PATH"/phabricator/conf/local/www.json
sudo chgrp www-data "$SCAP_REV_PATH"/phabricator/conf/local/local.json

sudo chgrp www-data "$SCAP_REV_PATH"/phabricator/support/redirect_config.json

sudo chmod 0440 "$SCAP_REV_PATH"/phabricator/conf/local/*.json
sudo chmod a+r "$SCAP_REV_PATH/phabricator/conf/local/local.json"
sudo chmod 0440 "$SCAP_REV_PATH"/phabricator/support/*.json
